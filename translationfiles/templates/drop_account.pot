# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Nextcloud package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Nextcloud 3.14159\n"
"Report-Msgid-Bugs-To: translations\\@example.com\n"
"POT-Creation-Date: 2021-01-07 15:13+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: /home/tcit/dev/nextcloud/server/apps/drop_account/lib/Activity/Provider.php:103
#, php-format
msgid "%1$s (%2$s - %3$s) deleted their account"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/lib/Activity/Provider.php:105
#: /home/tcit/dev/nextcloud/server/apps/drop_account/lib/Activity/Provider.php:107
#, php-format
msgid "%1$s (%2$s) deleted their account"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/lib/Activity/Provider.php:109
#, php-format
msgid "%1$s deleted their account"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/lib/Activity/Setting.php:52
msgid "An user <strong>deleted</strong> its account"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/lib/Controller/AccountController.php:107
msgid "User-Session unexpectedly expired"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/lib/Controller/AccountController.php:115
msgid "Successfully sent email"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/lib/Controller/AccountController.php:119
msgid "You have no email set up into your account"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/lib/Controller/AccountController.php:124
msgid "Unexpected error sending email. Please contact your administrator."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/lib/Service/ConfirmationService.php:121
#, php-format
msgid "Confirm your account deletion on %s"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/lib/Service/ConfirmationService.php:123
#, php-format
msgid "Account deletion confirmation for %s"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/lib/Service/ConfirmationService.php:124
msgid "Hello,"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/lib/Service/ConfirmationService.php:125
#, php-format
msgid "Someone - probably you - asked to delete their account on %s."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/lib/Service/ConfirmationService.php:126
msgid "To confirm the account deletion, you may click on the button below."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/lib/Service/ConfirmationService.php:127
#: /home/tcit/dev/nextcloud/server/apps/drop_account/lib/Settings/PersonalSection.php:64
msgid "Delete account"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/lib/Service/ConfirmationService.php:128
msgid "Cheers!"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialAppInfoFakeDummyForL10nScript.php:2
msgid "User account deletion"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialAppInfoFakeDummyForL10nScript.php:3
msgid "An app to allow users to delete their accounts."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialAppInfoFakeDummyForL10nScript.php:4
msgid ""
"# Description\n"
"\n"
"This app allows users to delete their accounts by removing all their data.\n"
"\n"
"It can also send activities to the admins about deleted accounts."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:1
msgid "Account deletion"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:2
msgid "Allows users to delete themselves their own account."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:3
msgid "Email confirmation"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:4
msgid "Require confirmation by email"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:5
msgid ""
"Will require users to click a confirmation link sent by email to confirm "
"their action."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:6
msgid "Data purge"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:7
msgid ""
"Users are not removed right away but disabled until a background job removed "
"their data for good. In the meanwhile, admins can \"save\" users by enabling "
"them back."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:8
msgid "Purge user data as soon as possible."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:9
msgid ""
"Uses the next background job available to completely remove the user's data."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:10
msgid "Purge user data after a while."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:11
msgid "Deletes user data after a grace period."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:12
msgid "Grace period duration"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:13
msgid "One day"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:14
msgid "One week"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:15
msgid "One month"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:16
msgid "Error while changing require confirmation setting"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:17
msgid "Error while changing delay purge setting"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:18
msgid "Error while changing delay purge hours setting"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:19
#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:34
msgid "Delete my account"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:20
msgid ""
"Deleting your account will delete all your files and data from the apps you "
"use, such as calendar and contacts."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:21
msgid "Account marked for deletion"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:22
msgid "Your account has been disabled and the data will be removed shortly."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:23
msgid "You are going to be redirected to the login page in a few seconds…"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:24
msgid "Email confirmation required"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:25
msgid ""
"Please click the link into the email we've just sent you to finish deleting "
"your account."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:26
msgid "This action is irreversible!"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:27
msgid ""
"After confirming the deletion of your account, you will be redirected to the "
"login page."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:28
msgid "You are the only user of this instance, you can't delete your account."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:29
msgid "You are the only admin of this instance, you can't delete your account."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:30
msgid ""
"An email confirmation is required by your admin to delete your account. "
"Please fill your email in your personal settings first."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:31
msgid "Do you really wish to delete your account?"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:32
msgid "We will send you an email to confirm this action."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:33
msgid "Check this to confirm the deletion request"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:35
msgid "Deleting your data…"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/specialVueFakeDummyForL10nScript.js:36
msgid "Error while deleting the account"
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/templates/account-deleted.php:5
msgid ""
"Your account has been marked for deletion. You can now close this window."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/templates/account-deleted.php:8
msgid "Account not found."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/templates/account-deleted.php:11
msgid "The token provided was not found."
msgstr ""

#: /home/tcit/dev/nextcloud/server/apps/drop_account/templates/account-deleted.php:14
msgid "Unknown error."
msgstr ""
