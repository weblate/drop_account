<div class="update">
	<p class="message">
		<?php switch ($_['status']) {
			case 'deleted':
				p($l->t('Your account has been marked for deletion. You can now close this window.'));
				break;
			case 'not-found':
				p($l->t('Account not found.'));
				break;
			case 'invalid-token':
				p($l->t('The token provided was not found.'));
				break;
			default:
				p($l->t('Unknown error.'));
				break;
		} ?>
	</p>
</div>
