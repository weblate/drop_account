<?php
/**
 * @copyright Copyright (c) 2020 Thomas Citharel <nextcloud@tcit.fr>
 *
 * @author Thomas Citharel <nextcloud@tcit.fr>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\DropAccount\Event;

use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\BackgroundJob\DeleteAccountData;
use OCP\BackgroundJob\IJobList;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\GenericEvent;
use OCP\EventDispatcher\IEventListener;
use OCP\IConfig;
use OCP\IUser;

class ReactivateUserListener implements IEventListener {


	/**
	 * @var IJobList
	 */
	private $jobList;
	/**
	 * @var IConfig
	 */
	private $config;

	public function __construct(IJobList $jobList, IConfig $config) {
		$this->jobList = $jobList;
		$this->config = $config;
	}

	public function handle(Event $event): void {
		if (!$event instanceof GenericEvent) {
			return;
		}

		/** @var IUser $user */
		$user = $event->getSubject();
		$uid = $user->getUID();
		$feature = $event->getArgument('feature');
		$oldValue = $event->getArgument('oldValue');
		$value = $event->getArgument('value');

		// We only do things on status being enabled
		if ($feature === 'enabled' && $value === true && $oldValue === false) {
			// Clear dedicated job if we have one
			if ($this->jobList->has(DeleteAccountData::class, ['uid' => $uid])) {
				$this->jobList->remove(DeleteAccountData::class, ['uid' => $uid]);
			}
			// Always clear purge values anyway
			$this->config->deleteUserValue($uid, Application::APP_NAME, 'markedForPurge');
			$this->config->deleteUserValue($uid, Application::APP_NAME, 'purgeDate');
		}
	}
}
