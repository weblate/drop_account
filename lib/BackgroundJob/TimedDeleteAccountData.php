<?php
/**
 * @copyright Copyright (c) 2017 Thomas Citharel <nextcloud@tcit.fr>
 *
 * @author Thomas Citharel <nextcloud@tcit.fr>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\DropAccount\BackgroundJob;

use DateInterval;
use DateTime;
use Exception;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\Service\DeleteAccountDataService;
use OCP\AppFramework\Utility\ITimeFactory;
use OCP\BackgroundJob\TimedJob;
use OCP\IConfig;

class TimedDeleteAccountData extends TimedJob {

	/** @var DeleteAccountDataService */
	protected $service;
	/**
	 * @var IConfig
	 */
	private $config;

	public function __construct(ITimeFactory $time, IConfig $config, DeleteAccountDataService $service) {
		parent::__construct($time);
		$this->service = $service;
		$this->config = $config;

		parent::setInterval(3600);
	}

	/**
	 * @param $argument
	 * @return mixed|void
	 * @throws Exception
	 */
	public function run($argument) {
		$userIds = $this->config->getUsersForUserValue(Application::APP_NAME, 'markedForPurge', 'yes');
		$purgePeriod = $this->config->getAppValue(Application::APP_NAME, 'userPurgePeriod', '24');

		foreach ($userIds as $uid) {
			$after = $this->config->getUserValue($uid, Application::APP_NAME, 'purgeDate', null);
			$after = (int) $after;
			$after = (new DateTime())->setTimestamp($after);
			$after->add(new DateInterval('PT' . $purgePeriod . 'H'));
			if ($after < new DateTime()) {
				$this->service->delete($uid);
			}
		}
	}
}
