<?php

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Activity;

use ChristophWurst\Nextcloud\Testing\TestCase;
use InvalidArgumentException;
use OCA\DropAccount\Activity\Provider;
use OCA\DropAccount\AppInfo\Application;
use OCP\Activity\IEvent;
use OCP\Activity\IEventMerger;
use OCP\Activity\IManager;
use OCP\IL10N;
use OCP\IURLGenerator;
use OCP\L10N\IFactory;
use PHPUnit\Framework\MockObject\MockObject;

class ProviderTest extends TestCase {
	/**
	 * @var IFactory|MockObject
	 */
	private $languageFactory;
	/**
	 * @var IURLGenerator|MockObject
	 */
	private $urlGenerator;
	/**
	 * @var IEventMerger|MockObject
	 */
	private $eventMerger;
	/**
	 * @var Provider
	 */
	private $provider;
	/**
	 * @var IL10N|MockObject
	 */
	private $l10n;

	public function setUp(): void {
		parent::setUp();
		$this->languageFactory = $this->createMock(IFactory::class);
		$this->urlGenerator = $this->createMock(IURLGenerator::class);
		$this->eventMerger = $this->createMock(IEventMerger::class);
		$eventManager = $this->createMock(IManager::class);
		$this->l10n = $this->createMock(IL10N::class);
		$this->l10n->expects($this->any())
			->method('t')
			->willReturnCallback(function ($string, $args) {
				return 'TRANSLATED: ' . vsprintf($string, $args);
			});
		$this->provider = new Provider($this->languageFactory, $this->urlGenerator, $this->eventMerger, $eventManager);
	}

	public function testProvideActivityFromOtherApp() {
		$event = $this->createMock(IEvent::class);
		$event->expects($this->once())->method('getApp')->willReturn('otherapp');
		$this->expectException('InvalidArgumentException');
		$this->provider->parse('en', $event);
	}

	public function testProvideActivityFromOtherType() {
		$event = $this->createMock(IEvent::class);
		$event->expects($this->once())->method('getApp')->willReturn(Application::APP_NAME);
		$event->expects($this->once())->method('getType')->willReturn('other_type');
		$this->expectException('InvalidArgumentException');
		$this->provider->parse('en', $event);
	}

	public function testParseWithNoSubjectParameters() {
		$event = $this->setupEvent();
		$event->expects($this->once())->method('getSubjectParameters')->willReturn(['email' => 'olduser@email.com']);

		$this->expectException(InvalidArgumentException::class);
		$this->expectExceptionMessage('Missing required username in parameters');

		$this->provider->parse('en', $event);
	}

	/**
	 * @dataProvider dataForTestParse
	 * @param array $parameters
	 * @param string $output
	 */
	public function testParse(array $parameters, string $output) {
		$event = $this->setupEvent();
		$event->expects($this->once())->method('getSubjectParameters')->willReturn($parameters);
		$event->expects($this->once())->method('setParsedSubject')->with($output);
		$finalEvent = $this->createMock(IEvent::class);
		$this->eventMerger->expects($this->once())->method('mergeEvents')->with('username', $event, null)->willReturn($finalEvent);

		$this->provider->parse('en', $event);
	}

	public function dataForTestParse(): array {
		return [
			[
				['username' => 'bye'], 'TRANSLATED: bye deleted their account',
			],
			[
				['username' => 'bye', 'name' => 'Dummy Account'], 'TRANSLATED: Dummy Account (bye) deleted their account'
			],
			[
				['email' => 'bye@me.com', 'name' => 'Dummy Account'], 'TRANSLATED: Dummy Account (bye@me.com) deleted their account'
			],
			[
				['email' => 'bye@me.com', 'name' => 'Dummy Account', 'username' => 'bye'], 'TRANSLATED: Dummy Account (bye - bye@me.com) deleted their account'
			]
		];
	}

	private function setupEvent() {
		$event = $this->createMock(IEvent::class);
		$event->expects($this->once())->method('getApp')->willReturn(Application::APP_NAME);
		$event->expects($this->once())->method('getType')->willReturn('account_deletion');

		$this->languageFactory->expects($this->once())->method('get')->with('drop_account', 'en')->willReturn($this->l10n);
		$this->urlGenerator->expects($this->once())->method('imagePath')->with('core', 'actions/delete.svg')->willReturn('smth.png');
		return $event;
	}
}
