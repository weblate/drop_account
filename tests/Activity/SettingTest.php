<?php

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Activity;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\DropAccount\Activity\Setting;
use OCP\IL10N;

class SettingTest extends TestCase {
	/**
	 * @var Setting
	 */
	private $setting;

	public function setUp(): void {
		parent::setUp();
		$l10n = $this->createMock(IL10N::class);
		$l10n->expects($this->any())
			->method('t')
			->willReturnCallback(function ($string, $args) {
				return 'TRANSLATED: ' . vsprintf($string, $args);
			});
		$this->setting = new Setting($l10n);
	}

	public function testGetIdentifier() {
		$this->assertEquals('account_deletion', $this->setting->getIdentifier());
	}

	public function testGetName() {
		$this->assertEquals('TRANSLATED: An user <strong>deleted</strong> its account', $this->setting->getName());
	}

	public function testGetPriority() {
		$this->assertEquals(80, $this->setting->getPriority());
	}

	public function testCanChangeStream() {
		$this->assertEquals(true, $this->setting->canChangeStream());
	}

	public function testIsDefaultEnabledStream() {
		$this->assertEquals(true, $this->setting->isDefaultEnabledStream());
	}

	public function testCanChangeMail() {
		$this->assertEquals(true, $this->setting->canChangeMail());
	}

	public function testIsDefaultEnabledMail() {
		$this->assertEquals(false, $this->setting->isDefaultEnabledMail());
	}
}
