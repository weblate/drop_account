<?php

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Service;

use BadMethodCallException;
use ChristophWurst\Nextcloud\Testing\TestUser;
use InvalidArgumentException;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\Service\DeleteAccountDataService;
use OCP\Activity\IEvent;
use OCP\Activity\IManager;
use OCP\IGroup;
use OCP\IGroupManager;
use OCP\ILogger;
use OCP\IUser;
use OCP\IUserManager;
use PHPUnit\Framework\MockObject\MockObject;
use ChristophWurst\Nextcloud\Testing\TestCase;

class DeleteAccountDataServiceTest extends TestCase {
	use TestUser;

	/**
	 * @var IManager|MockObject
	 */
	private $activityManager;
	/**
	 * @var IGroupManager|MockObject
	 */
	private $groupManager;
	/**
	 * @var IUserManager|MockObject
	 */
	private $userManager;
	/**
	 * @var DeleteAccountDataService
	 */
	private $deleteAccountDataService;
	/**
	 * @var IGroup|MockObject
	 */
	private $adminGroup;
	/**
	 * @var IUser|MockObject
	 */
	private $user;
	/**
	 * @var MockObject|ILogger
	 */
	private $logger;

	public function setUp(): void {
		parent::setUp();
		$this->userManager = $this->createMock(IUserManager::class);
		$this->logger = $this->createMock(ILogger::class);
		$this->groupManager = $this->createMock(IGroupManager::class);
		$this->activityManager = $this->createMock(IManager::class);

		$this->user = $this->createMock(IUser::class);
		$this->user->method('getUID')->willReturn('someuid');
		$this->user->method('getDisplayName')->willReturn(null);
		$this->user->method('getEMailAddress')->willReturn(null);
		$this->adminGroup = $this->createMock(IGroup::class);

		$this->deleteAccountDataService = new DeleteAccountDataService($this->userManager, $this->logger, $this->groupManager, $this->activityManager);
	}

	public function testDelete(): void {
		$this->setupDelete();

		$this->user->expects($this->once())->method('delete')->with()->willReturn(true);

		$this->deleteAccountDataService->delete('someuid');
	}

	public function testDeleteWithDeleteError(): void {
		$this->setupDelete();

		$this->user->expects($this->once())->method('delete')->with()->willReturn(false);
		$this->logger->expects($this->once())->method('error')->with('There has been an issue while deleting the user <someuid>.');

		$this->deleteAccountDataService->delete('someuid');
	}

	public function testDeleteWhenUserIsStillEnabled(): void {
		$this->userManager->expects($this->once())->method('get')->with('someuid')->willReturn($this->user);

		$this->user->expects($this->once())->method('isEnabled')->with()->willReturn(true);
		$this->logger->expects($this->once())->method('error')->with("Tried to delete the user <someuid>, but their account is still active.");

		$this->deleteAccountDataService->delete('someuid');
	}

	public function dataProviderTestDeleteWithActivityException(): array {
		return [
			[ new InvalidArgumentException()],
			[ new BadMethodCallException()],
		];
	}

	public function testDeleteWithActivityException(): void {
		$exception = new InvalidArgumentException();

		$admin1 = $this->createMock(IUser::class);

		$this->setupDelete([$admin1]);

		$event = $this->createMock(IEvent::class);
		$this->activityManager->expects($this->once())->method('generateEvent')->with()->willReturn($event);
		$event->expects($this->once())->method('setApp')->willThrowException($exception);

		$this->logger->expects($this->once())->method('logException')->with($exception, ['message' => 'There has been an issue sending the delete activity to admins', 'app' => Application::APP_NAME]);

		$this->deleteAccountDataService->delete('someuid');
	}

	public function testDeleteWithActivityManagerException(): void {
		$exception = new BadMethodCallException();

		$admin1 = $this->createMock(IUser::class);
		$admin1->expects($this->once())->method('getUID')->with()->willReturn('adminuid');

		$this->setupDelete([$admin1]);

		$this->user->expects($this->once())->method('delete')->with()->willReturn(true);
		$this->activityManager->expects($this->once())->method('publish')->willThrowException($exception);

		$this->logger->expects($this->once())->method('logException')->with($exception, ['message' => 'There has been an issue sending the delete activity to admins', 'app' => Application::APP_NAME]);

		$this->deleteAccountDataService->delete('someuid');
	}

	public function testDeleteWithActivities(): void {
		$admin1 = $this->createMock(IUser::class);
		$admin1->expects($this->once())->method('getUID')->with()->willReturn('adminuid');

		$this->setupDelete([$admin1]);

		$activityEvent = $this->getActivityEventMock('adminuid', 'someuid', null, null);

		$this->activityManager->expects($this->once())->method('publish')->with($activityEvent);

		$this->user->expects($this->once())->method('delete')->with()->willReturn(true);

		$this->deleteAccountDataService->delete('someuid');
	}

	private function getActivityEventMock(string $adminUid, string $uid, ?string $name, ?string $email) {
		$event = $this->createMock(IEvent::class);
		$this->activityManager->expects($this->once())->method('generateEvent')->with()->willReturn($event);
		$event->expects($this->once())
			->method('setApp')
			->with(Application::APP_NAME)
			->willReturn($event);

		$event->expects($this->once())
			->method('setType')
			->with('account_deletion')
			->willReturn($event);

		$event->expects($this->once())
			->method('setAuthor')
			->with($uid)
			->willReturn($event);

		$event->expects($this->once())
			->method('setSubject')
			->with('account_self_deletion', ['username' => $uid, 'name' => $name, 'email' => $email])
			->willReturn($event);

		$event->expects($this->once())
			->method('setAffectedUser')
			->with($adminUid)
			->willReturn($event);

		return $event;
	}

	private function setupDelete($users = []): void {
		$this->userManager->expects($this->once())->method('get')->with('someuid')->willReturn($this->user);
		$this->groupManager->expects($this->once())->method('get')->with('admin')->willReturn($this->adminGroup);
		$this->adminGroup->expects($this->once())->method('getUsers')->with()->willReturn($users);
	}
}
