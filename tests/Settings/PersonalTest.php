<?php

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Settings;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\Settings\Personal;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\IConfig;
use OCP\IGroup;
use OCP\IGroupManager;
use OCP\IInitialStateService;
use OCP\IUser;
use OCP\IUserManager;
use OCP\IUserSession;
use PHPUnit\Framework\MockObject\MockObject;

class PersonalTest extends TestCase {
	/**
	 * @var Personal
	 */
	private $personal;
	/**
	 * @var IUserManager|MockObject
	 */
	private $userManager;
	/**
	 * @var IUserSession|MockObject
	 */
	private $userSession;
	/**
	 * @var IGroupManager|MockObject
	 */
	private $groupManager;
	/**
	 * @var IInitialStateService|MockObject
	 */
	private $initialState;
	/**
	 * @var IConfig|MockObject
	 */
	private $config;

	public function setUp(): void {
		parent::setUp();
		$this->userSession = $this->createMock(IUserSession::class);
		$this->userManager = $this->createMock(IUserManager::class);
		$this->groupManager = $this->createMock(IGroupManager::class);
		$this->config = $this->createMock(IConfig::class);
		$this->initialState = $this->createMock(IInitialStateService::class);

		$this->personal = new Personal($this->config, $this->userSession, $this->userManager, $this->groupManager, $this->initialState);
	}

	public function testGetPriority() {
		$this->assertEquals(40, $this->personal->getPriority());
	}

	public function testGetSection() {
		$this->assertEquals(Application::APP_NAME, $this->personal->getSection());
	}

	/**
	 * @param int $nbUsers
	 * @param int $nbAdmins
	 * @param string $uid
	 * @param bool $isAdmin
	 * @param bool $requiresConfirmation
	 * @param string|null $email
	 * @param array $result
	 * @dataProvider dataForTestGetForm
	 */
	public function testGetForm(int $nbUsers, int $nbAdmins, string $uid, bool $isAdmin, bool $requiresConfirmation, ?string $email, array $result) {
		$res = new TemplateResponse(Application::APP_NAME, 'personal');

		$this->userManager->expects($this->once())->method('countUsers')->willReturn($nbUsers);
		$groupAdmin = $this->createMock(IGroup::class);
		$groupAdmin->expects($this->once())->method('count')->willReturn($nbAdmins);
		$this->groupManager->expects($this->once())->method('get')->with('admin')->willReturn($groupAdmin);

		$user = $this->createMock(IUser::class);
		$this->userSession->expects($this->once())->method('getUser')->willReturn($user);

		$this->config->expects($this->once())->method('getAppValue')->with(Application::APP_NAME, 'requireConfirmation', 'no')->willReturn($requiresConfirmation ? 'yes' : 'no');

		if ($requiresConfirmation) {
			$user->expects($this->exactly($email === null ? 1 : 2))->method('getEMailAddress')->willReturn($email);
		}

		if ($nbAdmins < 2) {
			$user->expects($this->once())->method('getUID')->willReturn($uid);
			$this->groupManager->expects($this->once())->method('isAdmin')->with($uid)->willReturn($isAdmin);
		}
		$this->initialState->expects($this->at(0))->method('provideInitialState')->with(Application::APP_NAME, 'has_email_for_confirmation', $result['has_email_for_confirmation']);
		$this->initialState->expects($this->at(1))->method('provideInitialState')->with(Application::APP_NAME, 'only_user', $result['only_user']);
		$this->initialState->expects($this->at(2))->method('provideInitialState')->with(Application::APP_NAME, 'only_admin', $result['only_admin']);
		$this->initialState->expects($this->at(3))->method('provideInitialState')->with(Application::APP_NAME, 'require_confirmation', $result['require_confirmation']);
		$this->assertEquals($res, $this->personal->getForm());
	}

	public function dataForTestGetForm(): array {
		return [
			[
				1, 1, 'bye', true, false, null, ['only_user' => true, 'only_admin' => true, 'has_email_for_confirmation' => false, 'require_confirmation' => false]
			],
			[
				1, 1, 'bye', false, false, null, ['only_user' => true, 'only_admin' => false, 'has_email_for_confirmation' => false, 'require_confirmation' => false]
			],
			[
				1, 2, 'bye', true, false, null, ['only_user' => true, 'only_admin' => false, 'has_email_for_confirmation' => false, 'require_confirmation' => false]
			],
			[
				10, 1, 'bye', true, false, null, ['only_user' => false, 'only_admin' => true, 'has_email_for_confirmation' => false, 'require_confirmation' => false]
			],
			[
				10, 1, 'bye', true, true, null, ['only_user' => false, 'only_admin' => true, 'has_email_for_confirmation' => false, 'require_confirmation' => true]
			],
			[
				10, 1, 'bye', true, true, '', ['only_user' => false, 'only_admin' => true, 'has_email_for_confirmation' => false, 'require_confirmation' => true]
			],
			[
				10, 1, 'bye', true, true, 'toto@me.com', ['only_user' => false, 'only_admin' => true, 'has_email_for_confirmation' => true, 'require_confirmation' => true]
			]
		];
	}
}
