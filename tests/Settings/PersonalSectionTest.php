<?php

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Settings;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\Settings\PersonalSection;
use OCP\IL10N;
use OCP\IURLGenerator;
use PHPUnit\Framework\MockObject\MockObject;

class PersonalSectionTest extends TestCase {

	/**
	 * @var PersonalSection
	 */
	private $personalSection;
	/**
	 * @var IURLGenerator|MockObject
	 */
	private $urlGenerator;

	public function setUp(): void {
		parent::setUp();
		$this->urlGenerator = $this->createMock(IURLGenerator::class);
		$l10n = $this->createMock(IL10N::class);
		$l10n->expects($this->any())
			->method('t')
			->willReturnCallback(function ($string, $args) {
				return 'TRANSLATED: ' . vsprintf($string, $args);
			});

		$this->personalSection = new PersonalSection($this->urlGenerator, $l10n);
	}

	public function testGetID() {
		$this->assertEquals(Application::APP_NAME, $this->personalSection->getID());
	}

	public function testGetName() {
		$this->assertEquals('TRANSLATED: Delete account', $this->personalSection->getName());
	}

	public function testGetPriority() {
		$this->assertEquals(90, $this->personalSection->getPriority());
	}

	public function testGetIcon() {
		$this->urlGenerator->expects($this->once())->method('imagePath')->with('core', 'actions/delete.svg');
		$this->personalSection->getIcon();
	}
}
