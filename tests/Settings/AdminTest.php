<?php

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Settings;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\Settings\Admin;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\IConfig;
use OCP\IInitialStateService;
use PHPUnit\Framework\MockObject\MockObject;

class AdminTest extends TestCase {
	/**
	 * @var Admin
	 */
	private $admin;
	/**
	 * @var IInitialStateService|MockObject
	 */
	private $initialState;
	/**
	 * @var IConfig|MockObject
	 */
	private $config;

	public function setUp(): void {
		parent::setUp();
		$this->config = $this->createMock(IConfig::class);
		$this->initialState = $this->createMock(IInitialStateService::class);

		$this->admin = new Admin($this->config, $this->initialState);
	}

	public function testGetPriority() {
		$this->assertEquals(80, $this->admin->getPriority());
	}

	public function testGetSection() {
		$this->assertEquals('additional', $this->admin->getSection());
	}

	/**
	 * @param string $requiresConfirmation
	 * @param string $delayPurge
	 * @param string $delayPurgeHours
	 * @param array $result
	 * @dataProvider dataForTestGetForm
	 */
	public function testGetForm(string $requiresConfirmation, string $delayPurge, string $delayPurgeHours, array $result) {
		$res = new TemplateResponse(Application::APP_NAME, 'admin');

		$this->config->expects($this->at(0))->method('getAppValue')->with(Application::APP_NAME, 'requireConfirmation', 'no')->willReturn($requiresConfirmation);
		$this->config->expects($this->at(1))->method('getAppValue')->with(Application::APP_NAME, 'delayPurge', 'no')->willReturn($delayPurge);
		$this->config->expects($this->at(2))->method('getAppValue')->with(Application::APP_NAME, 'delayPurgeHours', '24')->willReturn($delayPurgeHours);

		$this->initialState->expects($this->at(0))->method('provideInitialState')->with(Application::APP_NAME, 'requireConfirmation', $result['requireConfirmation']);
		$this->initialState->expects($this->at(1))->method('provideInitialState')->with(Application::APP_NAME, 'delayPurge', $result['delayPurge']);
		$this->initialState->expects($this->at(2))->method('provideInitialState')->with(Application::APP_NAME, 'delayPurgeHours', $result['delayPurgeHours']);
		$this->assertEquals($res, $this->admin->getForm());
	}

	public function dataForTestGetForm(): array {
		return [
			[
				'no', 'no', '24', ['requireConfirmation' => false, 'delayPurge' => 'no', 'delayPurgeHours' => '24']
			],
			[
				'yes', 'no', '24', ['requireConfirmation' => true, 'delayPurge' => 'no', 'delayPurgeHours' => '24']
			],
			[
				'yes', 'yes', '24', ['requireConfirmation' => true, 'delayPurge' => 'yes', 'delayPurgeHours' => '24']
			],
			[
				'yes', 'yes', '720', ['requireConfirmation' => true, 'delayPurge' => 'yes', 'delayPurgeHours' => '720']
			],
			[
				'no', 'yes', '168', ['requireConfirmation' => false, 'delayPurge' => 'yes', 'delayPurgeHours' => '168']
			],
		];
	}
}
