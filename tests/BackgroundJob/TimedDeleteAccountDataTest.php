<?php

declare(strict_types=1);

namespace OCA\DropAccount\Tests\BackgroundJob;

use ChristophWurst\Nextcloud\Testing\TestCase;
use DateTime;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\BackgroundJob\TimedDeleteAccountData;
use OCA\DropAccount\Service\DeleteAccountDataService;
use OCP\AppFramework\Utility\ITimeFactory;
use OCP\IConfig;
use PHPUnit\Framework\MockObject\MockObject;

class TimedDeleteAccountDataTest extends TestCase {

	/**
	 * @var TimedDeleteAccountData
	 */
	private $timedDeleteAccountDataJob;
	/**
	 * @var DeleteAccountDataService|MockObject
	 */
	private $service;
	/**
	 * @var IConfig|MockObject
	 */
	private $config;

	public function setUp(): void {
		parent::setUp();
		$timeFactory = $this->createMock(ITimeFactory::class);
		$this->config = $this->createMock(IConfig::class);
		$this->service = $this->createMock(DeleteAccountDataService::class);
		$this->timedDeleteAccountDataJob = new TimedDeleteAccountData($timeFactory, $this->config, $this->service);
	}

	public function test() {
		$config = [['uid' => 'uid1', 'date' => null], ['uid' => 'uid2', 'date' => (string) (new DateTime())->getTimestamp()]];
		$configMap = array_map(function ($key) {
			return array_merge([$key['uid']], [Application::APP_NAME, 'purgeDate', null]);
		}, $config);
		$uids = array_map(function ($key) {
			return $key['uid'];
		}, $config);
		$dates = array_map(function ($key) {
			return $key['date'];
		}, $config);

		$this->config->expects($this->once())->method('getUsersForUserValue')->with(Application::APP_NAME, 'markedForPurge', 'yes')->willReturn($uids);
		$this->config->expects($this->once())->method('getAppValue')->with(Application::APP_NAME, 'userPurgePeriod', '24')->willReturn('24');

		$this->config->expects($this->exactly(count($uids)))->method('getUserValue')->withConsecutive(...$configMap)->willReturnOnConsecutiveCalls(...$dates);
		$this->service->expects($this->atMost(1))->method('delete')->with($uids[0]);

		$this->timedDeleteAccountDataJob->run([]);
	}
}
