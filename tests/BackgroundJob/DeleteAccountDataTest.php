<?php

declare(strict_types=1);

namespace OCA\DropAccount\Tests\BackgroundJob;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\DropAccount\BackgroundJob\DeleteAccountData;
use OCA\DropAccount\Service\DeleteAccountDataService;
use OCP\AppFramework\Utility\ITimeFactory;
use PHPUnit\Framework\MockObject\MockObject;

class DeleteAccountDataTest extends TestCase {
	/**
	 * @var DeleteAccountData
	 */
	private $deleteAccountDataJob;
	/**
	 * @var DeleteAccountDataService|MockObject
	 */
	private $service;

	public function setUp(): void {
		parent::setUp();
		$timeFactory = $this->createMock(ITimeFactory::class);
		$this->service = $this->createMock(DeleteAccountDataService::class);
		$this->deleteAccountDataJob = new DeleteAccountData($timeFactory, $this->service);
	}

	public function test() {
		$this->service->expects($this->once())->method('delete')->with('toto');
		$this->deleteAccountDataJob->run(['uid' => 'toto']);
	}

	public function testWithEmptyArray() {
		$this->expectExceptionMessage('DeleteAccountData job requires an uid argument');
		$this->deleteAccountDataJob->run([]);
	}
}
