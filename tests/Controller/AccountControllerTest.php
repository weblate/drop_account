<?php

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Controller;

use Exception;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\BackgroundJob\DeleteAccountData;
use OCA\DropAccount\Controller\AccountController;
use OCA\DropAccount\MissingEmailException;
use OCA\DropAccount\Service\ConfirmationService;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\BackgroundJob\IJobList;
use OCP\IConfig;
use OCP\IL10N;
use OCP\ILogger;
use OCP\IRequest;
use OCP\IUser;
use OCP\IUserSession;
use PHPUnit\Framework\MockObject\MockObject;
use ChristophWurst\Nextcloud\Testing\TestCase;

class AccountControllerTest extends TestCase {
	/** @var IUserSession|MockObject */
	private $userSession;

	/** @var IJobList|MockObject */
	private $jobList;

	/** @var IConfig|MockObject */
	private $config;

	/** @var ConfirmationService|MockObject */
	private $confirmationService;
	/**
	 * @var IUser|MockObject
	 */
	private $user;
	/**
	 * @var AccountController
	 */
	private $controller;

	public const USER_EMAIL = 'user@email.com';

	public function setUp(): void {
		parent::setUp();
		$request = $this->createMock(IRequest::class);
		$l10n = $this->createMock(IL10N::class);
		$this->userSession = $this->createMock(IUserSession::class);
		$logger = $this->createMock(ILogger::class);
		$this->jobList = $this->createMock(IJobList::class);
		$this->config = $this->createMock(IConfig::class);
		$this->confirmationService = $this->createMock(ConfirmationService::class);
		$this->user = $this->createMock(IUser::class);
		$this->user->method('getDisplayName')->willReturn('User Displayname 123');
		$this->user->method('getEMailAddress')->willReturn(self::USER_EMAIL);

		$l10n->expects($this->any())
			->method('t')
			->willReturnCallback(function ($string, $args) {
				return 'TRANSLATED: ' . vsprintf($string, $args);
			});

		$this->controller = new AccountController(
			Application::APP_NAME,
			$request,
			$this->userSession,
			$logger,
			$this->config,
			$l10n,
			$this->jobList,
			$this->confirmationService
		);
	}

	public function testUserSessionExpired() {
		$this->userSession->expects($this->once())
			->method('getUser')
			->with()
			->willReturn(null);

		$response = $this->controller->delete();
		$this->assertInstanceOf(JSONResponse::class, $response);
		$this->assertEquals([
			'message' => 'TRANSLATED: User-Session unexpectedly expired'
		], $response->getData());
		$this->assertEquals(401, $response->getStatus());
	}

	public function testSendConfirmationEmail() {
		$this->userSession->expects($this->once())
			->method('getUser')
			->with()
			->willReturn($this->user);

		$this->config->expects($this->once())
			->method('getAppValue')
			->with(Application::APP_NAME, 'requireConfirmation', 'no')
			->willReturn('yes');

		$this->confirmationService->expects($this->once())
			->method('sendConfirmationEmail')
			->with($this->user)
			->willReturn(self::USER_EMAIL);

		$response = $this->controller->delete();

		$this->assertInstanceOf(JSONResponse::class, $response);
		$this->assertEquals([
			'message' => 'TRANSLATED: Successfully sent email'
		], $response->getData());
		$this->assertEquals(Http::STATUS_CREATED, $response->getStatus());
	}

	public function testSendConfirmationEmailWithoutUserEmail() {
		$user = $this->createMock(IUser::class);
		$user->method('getEMailAddress')->willReturn(null);

		$this->userSession->expects($this->once())
			->method('getUser')
			->with()
			->willReturn($user);

		$this->config->expects($this->once())
			->method('getAppValue')
			->with(Application::APP_NAME, 'requireConfirmation', 'no')
			->willReturn('yes');

		$this->confirmationService->expects($this->once())
			->method('sendConfirmationEmail')
			->with($user)
			->willThrowException(new MissingEmailException());

		$response = $this->controller->delete();

		$this->assertInstanceOf(JSONResponse::class, $response);
		$this->assertEquals([
			'message' => 'TRANSLATED: You have no email set up into your account'
		], $response->getData());
		$this->assertEquals(Http::STATUS_BAD_REQUEST, $response->getStatus());
	}

	public function testSendConfirmationEmailWithoutMailerFailure() {
		$this->userSession->expects($this->once())
			->method('getUser')
			->with()
			->willReturn($this->user);

		$this->config->expects($this->once())
			->method('getAppValue')
			->with(Application::APP_NAME, 'requireConfirmation', 'no')
			->willReturn('yes');

		$this->confirmationService->expects($this->once())
			->method('sendConfirmationEmail')
			->with($this->user)
			->willThrowException(new Exception('Something bad happened'));

		$response = $this->controller->delete();

		$this->assertInstanceOf(JSONResponse::class, $response);
		$this->assertEquals([
			'message' => 'TRANSLATED: Unexpected error sending email. Please contact your administrator.'
		], $response->getData());
		$this->assertEquals(Http::STATUS_INTERNAL_SERVER_ERROR, $response->getStatus());
	}

	/**
	 * @dataProvider dataTestDeleteAccount
	 * @param string $delayPurge
	 */
	public function testDeleteAccount(string $delayPurge) {
		$this->user->expects($this->exactly($delayPurge === 'yes' ? 2 : 1))
			->method('getUID')
			->with()
			->willReturn('userid');

		$this->userSession->expects($this->once())
			->method('getUser')
			->with()
			->willReturn($this->user);

		$this->config
			->method('getAppValue')
			->withConsecutive([Application::APP_NAME, 'requireConfirmation', 'no'], [Application::APP_NAME, 'delayPurge', 'no'])
			->willReturnOnConsecutiveCalls('no', $delayPurge);

		$this->user->expects($this->once())
			->method('setEnabled')
			->with(false);

		if ($delayPurge === 'yes') {
			$this->config
				->expects($this->exactly(2))
				->method('setUserValue');
		} else {
			$this->jobList->expects($this->once())
				->method('add')
				->with(DeleteAccountData::class, ['uid' => 'userid']);
		}

		$response = $this->controller->delete();

		$this->assertInstanceOf(JSONResponse::class, $response);
		$this->assertEquals([], $response->getData());
		$this->assertEquals(Http::STATUS_ACCEPTED, $response->getStatus());
	}

	public function dataTestDeleteAccount(): array {
		return [
			[
				'no'
			],
			[
				'yes'
			]
		];
	}

	public function testConfirmWithoutUserSession() {
		$this->userSession->expects($this->once())->method('getUser')->willReturn(null);
		$response = $this->controller->confirm('some-token');

		$expectedResponse = new TemplateResponse(Application::APP_NAME, 'account-deleted', ['uid' => null, 'status' => 'not-found'], 'guest');
		$expectedResponse->setStatus(Http::STATUS_UNAUTHORIZED);
		$this->assertEquals($expectedResponse, $response);
	}

	public function testConfirmWithBadToken() {
		$this->user->expects($this->once())
			->method('getUID')
			->with()
			->willReturn('userid');
		$this->userSession->expects($this->once())->method('getUser')->willReturn($this->user);
		$this->config->expects($this->once())->method('getUserValue')->with('userid', Application::APP_NAME, 'delete_token', null)->willReturn('something-else');
		$response = $this->controller->confirm('some-token');

		$expectedResponse = new TemplateResponse(Application::APP_NAME, 'account-deleted', ['uid' => 'userid', 'status' => 'invalid-token'], 'guest');
		$expectedResponse->setStatus(Http::STATUS_NOT_FOUND);
		$this->assertEquals($expectedResponse, $response);
	}

	/**
	 * @param string $delayPurge
	 * @dataProvider dataTestConfirmAccount
	 */
	public function testConfirm(string $delayPurge) {
		$this->user->expects($this->exactly($delayPurge === 'yes' ? 3 : 2))
			->method('getUID')
			->with()
			->willReturn('userid');
		$this->userSession->expects($this->once())->method('getUser')->willReturn($this->user);
		$this->config->expects($this->once())->method('getUserValue')->with('userid', Application::APP_NAME, 'delete_token', null)->willReturn('some-token');

		$this->config
			->expects($this->once())
			->method('getAppValue')
			->with(Application::APP_NAME, 'delayPurge', 'no')
			->willReturn($delayPurge);

		$this->user->expects($this->once())
			->method('setEnabled')
			->with(false);

		if ($delayPurge === 'yes') {
			$this->config
				->expects($this->exactly(2))
				->method('setUserValue');
		} else {
			$this->jobList->expects($this->once())
				->method('add')
				->with(DeleteAccountData::class, ['uid' => 'userid']);
		}

		$response = $this->controller->confirm('some-token');

		$expectedResponse = new TemplateResponse(Application::APP_NAME, 'account-deleted', ['uid' => 'userid', 'status' => 'deleted'], 'guest');
		$this->assertEquals($expectedResponse, $response);
	}

	public function dataTestConfirmAccount(): array {
		return [
			[
				'no'
			],
			[
				'yes'
			]
		];
	}
}
