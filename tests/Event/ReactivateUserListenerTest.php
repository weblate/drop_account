<?php

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Service;

use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\BackgroundJob\DeleteAccountData;
use OCA\DropAccount\Event\ReactivateUserListener;
use OCP\BackgroundJob\IJobList;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\GenericEvent;
use OCP\IConfig;
use OCP\IUser;
use PHPUnit\Framework\MockObject\MockObject;
use ChristophWurst\Nextcloud\Testing\TestCase;

class ReactivateUserListenerTest extends TestCase {
	/**
	 * @var ReactivateUserListener
	 */
	private $reactivateUserListener;
	/**
	 * @var IConfig|MockObject
	 */
	private $config;
	/**
	 * @var IJobList|MockObject
	 */
	private $jobList;

	public function setUp(): void {
		$this->jobList = $this->createMock(IJobList::class);
		$this->config = $this->createMock(IConfig::class);
		$this->reactivateUserListener = new ReactivateUserListener($this->jobList, $this->config);
	}

	public function testWithBadEvent() {
		$event = new Event();
		$this->jobList->expects($this->never())->method('has');
		$this->reactivateUserListener->handle($event);
	}

	/**
	 * @dataProvider dataTestReactivateUserListenerWhenUserIsDisabled
	 * @param $feature
	 * @param $oldValue
	 * @param $newValue
	 * @param bool $hasJob
	 * @param bool $perform
	 */
	public function testReactivateUserListenerWhenUserIsDisabled($feature, $oldValue, $newValue, bool $hasJob, bool $perform) {
		$user = $this->createMock(IUser::class);
		$user->expects($this->once())->method('getUID')->willReturn('someuid');
		$event = new GenericEvent($user, ['feature' => $feature, 'oldValue' => $oldValue, 'value' => $newValue]);

		if ($perform) {
			$this->jobList->expects($this->once())->method('has')->with(DeleteAccountData::class, ['uid' => 'someuid'])->willReturn($hasJob);
			if ($hasJob) {
				$this->jobList->expects($this->once())->method('remove')->with(DeleteAccountData::class, ['uid' => 'someuid']);
			}
			$this->config->expects($this->exactly(2))->method('deleteUserValue')->withConsecutive(['someuid', Application::APP_NAME, 'markedForPurge'], ['someuid', Application::APP_NAME, 'purgeDate']);
		}

		$this->reactivateUserListener->handle($event);
	}

	public function dataTestReactivateUserListenerWhenUserIsDisabled(): array {
		return [
			[
				'enabled', false, true, false, true
			],
			[
				'enabled', false, true, true, true
			],
			[
				'enabled', true, false, false, false
			],
			[
				'enabled', true, false, true, false
			]
		];
	}
}
